DELIMITER //
CREATE TRIGGER __before_insert_employee_post BEFORE INSERT ON employee FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM post WHERE post=new.post) = 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t insert record. Foreign post key does not exist!';
END IF;
END //
DELIMITER ;

delimiter //
create trigger _before_update_employee_post 
Before update on employee for each row
begin
 if(select count(*) from post where post = new.post)=0 then
     signal sqlstate '45000' set mysql_errno = 30001, message_text = 'Can\'t update record. Foreign post key doesn\'t exist';
  end if;
  end //
DELIMITER ;