delimiter // 
create trigger _after_update_post  
after update on post for each row
begin 
 update employee set employee.post = new.post where employee.post = old.post;
 end //
delimiter ; 
 
delimiter // 
create trigger _after_delete_post  
after delete on post for each row
begin 
 delete from employee   where employee.post = old.post;
end //
delimiter ; 

