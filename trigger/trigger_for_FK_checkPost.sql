DELIMITER //
CREATE TRIGGER __before_update_post BEFORE UPDATE ON post FOR EACH ROW
BEGIN
IF ( old.post <> new.post AND (SELECT COUNT(*) FROM employee WHERE employee.post = old.post) <> 0 ) THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign key updates to employee table restricted!';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER __before_delete_parent BEFORE DELETE ON post FOR EACH ROW
BEGIN
IF ( SELECT COUNT(*) FROM employee WHERE employee.post = old.post) <> 0 THEN
    SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. Foreign key exists in employee table!';
END IF;
END //
DELIMITER ;