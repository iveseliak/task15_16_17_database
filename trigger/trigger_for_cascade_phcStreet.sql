delimiter //
create trigger __after_update_street 
after update on street for each row
begin
  update pharmacy set pharmacy.street =new.street where pharmacy.street = old.street;
 end //
 delimiter ;
 
 delimiter //
create trigger __after_delete_street 
after delete on street for each row
begin
  delete from pharmacy  where pharmacy.street = old.street;
 end //
 delimiter ;
 
 