

delimiter //
CREATE  trigger _before_insert_forbitten
 before insert on post for each row
 begin 
 if(SELECT COUNT(*) FROM post WHERE post=new.post) =0 then
 signal sqlstate '45000'
 set message_text = 'Insert forbitten';
end if;
end //
delimiter ; 

delimiter //
create  trigger _before_update_forbitten
 before update on post for each row
 begin 
 if(SELECT COUNT(*) FROM post WHERE post=new.post) =0 then
 signal sqlstate '45000'
 set message_text = 'Update forbitten';
end if;
end //
delimiter ; 

delimiter //
create  trigger _before_delete_forbitten
 before delete on post for each row
 begin 
 if(SELECT COUNT(*) FROM post WHERE post.post=old.post) <>0 then
 signal sqlstate '45000'
 set message_text = 'Delete forbitten';
end if;
end //
delimiter ; 