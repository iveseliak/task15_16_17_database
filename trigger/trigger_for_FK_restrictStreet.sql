delimiter //
 create trigger _before_update_street
 before update on street for each row
 begin
 if(old.street <> new.street and (select count(*) from pharmacy where pharmacy.street = old.street)<> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign key updates to pharmacy table restricted!';
 end if;
 end //
 delimiter ; 
 
 delimiter //
 create trigger _before_delete_street
 before delete on street for each row
 begin
 if(select count(*) from pharmacy where pharmacy.street = old.street <> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. FForeign key exists in pharmacy table!';
 end if;
 end //
 delimiter ; 