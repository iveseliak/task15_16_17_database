delimiter //
create trigger _before_insert_identity_number
before insert on employee for each row
begin 
 if(new.identity_number  rlike '(00)$')
 THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'CHECK error for Insert';

END IF;
END //
DELIMITER ;