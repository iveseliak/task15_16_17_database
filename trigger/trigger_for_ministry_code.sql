delimiter //
create trigger _before_insert_ministry_code
before insert on medicine for each row
begin 
 if(new.ministry_code not rlike '^([A-LNOQ-Z])-([0-9]){3}-([0-9]){2}$')
 THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'CHECK error for Insert';

END IF;
END //
DELIMITER ;