delimiter //
use storedpr_db //
DROP PROCEDURE IF EXISTS CreateTableCursor// 

 create procedure CreateTableCursor()
 begin
   declare done int default false;
   declare NameEpm char(30);
   declare _Cursore cursor for
   select name from employee;
   
   declare continue handler for not found
   set done=true;
   
   open _Cursore;
   
   myLoop: loop 
          fetch _Cursore into NameEpm;
          if done = true then leave myLoop;
          end if;
          
          set @t_query = concat('create table IF NOT EXISTS', ' ' ,NameEpm , '(' , 'id int' , 'surname varchar(10)', 'identity_number char(10)',  ')' , ';' );
          
          prepare myQuery from @t_query;
          execute myQuery;
          deallocate prepare myQuery;
          
          end loop;
          close _Cursore;
    end //
    delimiter ;
    
    
call CreateTableCursor;
          