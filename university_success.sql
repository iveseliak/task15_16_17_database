create database university_success;
use university_success;
create table studet
(
  idStudent int   AUTO_INCREMENT PRIMARY KEY,
  firstname varchar(45) not null,
  lasrname varchar(45) not null,
  surname varchar(45) not null,
  foto longblob null,
  autobiograhy nvarchar(4000) null,
  year_entry year not null,
  birthday year not null,
  adress varchar(50) not null,
  rating int not null,
  studentship decimal(6,2) default 1452.12,
  idGroup_code int
  
)Engine InnoDB auto_increment 1;

create table group_code
(
  idGroup_code int primary key,
  codeGRP varchar(10) 
)engine InnoDB;

create table study_subject
(
 idSubject int primary key,
 nameSunject varchar(45) ,
 idGroup_code int
)engine InnoDB;

create table modul_one
(
 idModul_one int primary key,
 nameSub varchar(45),
 typeControl varchar(10),
 result_100 int,
 term int,
 result_5 int,
 idStudent int
 )engine InnoDB;
 
create table module_two
(
 idModul_two int primary key,
 nameSub varchar(45),
 typeControl varchar(10),
 result_100 int,
 term int,
 result_5 int,
 idStudent int
)engine InnoDB;

create table lecturer
(
   idLecturer int primary key,
   nameLecturer varchar(45),
   idSubject int

)engine InnoDB;

ALTER TABLE studet
ADD CONSTRAINT FK_student_groupCode
FOREIGN KEY ( idGroup_code)
REFERENCES group_code (idGroup_code)
ON DELETE CASCADE ON UPDATE SET NULL ;

ALTER TABLE study_subject
ADD CONSTRAINT FK_study_subject_groupCode
FOREIGN KEY ( idGroup_code)
REFERENCES group_code (idGroup_code)
ON DELETE CASCADE ON UPDATE SET NULL ;


ALTER TABLE modul_one
ADD CONSTRAINT FK_module_one_Student
FOREIGN KEY ( idStudent)
REFERENCES studet (idStudent)
ON DELETE CASCADE ON UPDATE SET NULL ;

ALTER TABLE module_two
ADD CONSTRAINT FK_module_two_Student
FOREIGN KEY ( idStudent)
REFERENCES studet (idStudent)
ON DELETE CASCADE ON UPDATE SET NULL ;

ALTER TABLE lecturer
ADD CONSTRAINT FK_lecturer_Subject
FOREIGN KEY ( idSubject)
REFERENCES study_subject (idSubject)
ON DELETE CASCADE ON UPDATE SET NULL ;


