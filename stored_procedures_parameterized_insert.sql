delimiter //
use storedpr_db //
DROP PROCEDURE IF EXISTS param_insert //
 create procedure param_insert( id  int, surname varchar(30), name char(30), midle_name  varchar(30),
                  identity_number char(10), passport  char(10), experience decimal(10, 1), birthday  date, 
                  post varchar(15) , pharmacy_id int)
  begin
   
   insert into employee  (id, surname, name, midle_name, identity_number, passport
						 , experience , birthday, post, pharmacy_id)
                 values (id, surname, name, midle_name, identity_number, passport
						 , experience , birthday, post, pharmacy_id) ;
                         
end //
delimiter ;
